const express = require('express');
const path = require('path');
const router = express.Router();

const feedparser = require('ortoo-feedparser');

//Try crawlers
const request = require('request');
const cheerio = require('cheerio');



const Emplois = require('../models/emplois');
const User = require('../models/User');
const Pub = require('../models/Post');
const Comm = require('../models/Comment');
const Actualite = require('../models/Actualite')


router.get('/feed', (req,res) => {
    var url = "https://www.iwnsvg.com/feed/";
    feedparser.parseUrl(url).on('article', (article)=> {
        console.log('title: ' , article.title );
    });
});



//post un commentaire
router.post('/postc',(req,res)=> {
    console.log('post comment')
    Comm.create(req.body)
    .then(cmt=>{
        console.log('commentaire ajouté');
        res.json(cmt);
    })
    .catch(err=>{
        console.log('commentaire n a pas été ajouté');
    })
});



//get les commentaire d'un post
router.get('/getallcom/:id',(req,res)=>{
    console.log('find comms by post');
    Comm.find({
        idpost : req.params.id
    })
    .then(com=>{
        console.log('commentaires trouvés');
        res.json(com);
    })
    .catch(err=>{
        console.log('commentaires not found');
        res.send(null);
    })
});


//supprimer un commentaire
router.delete('/removec/:id',(req,res)=>{
    Comm.findOneAndDelete({_id: req.params.id})
    .then(cmnt=>{   
        console.log(cmnt);
        console.log('commentaire supprimé !');
        res.send('ok');
    })
    .catch(err=>{
        console.log('erreur');
        res.send(null);
    })
});


//add like to a post
router.put('/like/:idpost/:idusr', (req,res) => {
    console.log('créer like');
    
    Pub.findOneAndUpdate({ _id : req.params.idpost } ,
        
         { $push: {likes : (req.params.idusr)} }
    
    )
    .then(pub => {
        console.log('like ajouté');
        res.send('ok');
    })
    .catch(err => {
        console.log('erreur like');
        res.send(null);
    })
});




//get les likes d'un post 
router.get('/postlikes/:id',(req,res) => {
    console.log('chercher les j aimes d un post ');

    Pub.findById(req.params.id)
    .then(post=>{
        console.log('like trouvé');
        res.json(post.likes);
    })
    .catch(err=>{
        console.log('erreur');
        res.send(null);
    })
});


//remove a like from a post 
router.put('/likeremove/:idpost/:idusr', (req,res) => {
    console.log('supp like');

    Pub.findOneAndUpdate({_id : req.params.idpost },
    
        {$pull: {likes : (req.params.idusr)}}

    )
    .then(pub => {
        console.log('like supprimé');
        res.send('ok');
    })
    .catch(err => {
        console.log('erreur like');
        res.send(err.message);
    })
});


//publier un fichier
router.post('/postpwithfile' , (req,res) => {
    if (req.files) {
        const {imgpost} = req.files;
        const fileName = `fileisgstudent${Date.now().toString()}${imgpost.name}`;
        imgpost.mv(path.resolve(__dirname, '..', `uploads/posts/files/${fileName}`), (error) => {
          Pub.create({
              ...req.body,
              imgpost: `posts/files/${fileName}`,
              likes : new Array()
            }) 
            .then(pub=>{
                console.log('post ajouté');
            })
            .catch(err=>{
                console.log('post n a pas été ajouté ')
            })

  });
}
  else {
      Pub.create(req.body)
      .then(post=>{
          console.log("post ajouté !")
          res.json(post)
      })
      .catch(err=>{
          console.log('post n a pas été ajouté !');
      })
  }
});



//post un post(une publication)
router.post('/postp', (req,res)=> {
    
    if (req.files) {

        let obj = req.files;
        Object.size = function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        };
        let list = new Array();
        let i=0 ;
        for (i ; i<Object.size(req.files) ; i++) {
        const imgName = `imageisgstudent${Date.now().toString()}${obj[Object.keys(obj)[i]].name}`;
        obj[Object.keys(obj)[i]].mv(path.resolve(__dirname, '..', `uploads/posts/images/${imgName}`));
        list.push(`posts/images/${imgName}`);
        }
        
        Pub.create({
            ...req.body,
            imgpost: list,
            likes : new Array()
        })
        .then(pst=>{
            console.log("post ajouté !"),
            res.send(pst.id)

        })
        .catch(err=>{
            console.log('post n a pas été ajouté !');
        })



    }
    else {
        Pub.create(req.body)
        .then(post=>{
            console.log("post ajouté !")
            res.json(post)
        })
        .catch(err=>{
            console.log('post n a pas été ajouté !');
        })
    }
});



//get all posts
router.get('/postg',(req,res)=>{
    console.log("getting all posts");
    Pub.find()
    .then(pst=>{
        res.json(pst)
    })
    .catch(err=>{
        res.send(err)
    })
});


//get one post by ID
router.get('/postg/:id',(req,res)=>{
    console.log("getting all posts");
    Pub.findOne({_id : req.params.id})
    .then(pst=>{
        res.json(pst)
    })
    .catch(err=>{
        res.send(err)
    })
});




//modifier un user
router.post('/userupdate' , (req,res) => {
    console.log('modifier un utilisateur');
    console.log(req.body);

    if (req.files) {
        const {img} = req.files;
    const imgName = `avatar${Date.now().toString()}${img.name}`;
    img.mv(path.resolve(__dirname, '..', `uploads/users/${imgName}`), (error) => {
        User.findOneAndUpdate({ _id: (req.body.id) , pass: (req.body.mdp) },{
           $set:{ nom :  (req.body.nom) ,
            img: `users/${imgName}`
        }
        })
        .then(usr=>{
            const o = {
                _id : usr.id,
                email : usr.email,
                nom : usr.nom,
                prenom : usr.prenom,
                filiere : usr.filiere,
                niveau : usr.niveau,
                groupe : usr.groupe,
                img : usr.img};
            console.log('ok modifié')
             res.json(o);
        })
        .catch(errdd=>{
            console.log('vérifier votre mot de passe')
            console.log(errdd.message)
            res.send("null")
        })
   
        });
    }

    else {
        User.findOneAndUpdate({ _id: req.body.id , pass: req.body.mdp },{
            $set:{ ...req.body
            } 
        })
        .then(usr =>{
            const o = {
                _id : usr.id,
                email : usr.email,
                nom : usr.nom,
                prenom : usr.prenom,
                filiere : usr.filiere,
                niveau : usr.niveau,
                groupe : usr.groupe,
                img : usr.img};
                console.log('ok modifié')
             res.json(o);
        })
        .catch(errdd=>{
            console.log('vérifier votre mot de passe')
            res.send('null')
        })
    }
});


router.post('/u', (req,res) => {
    const {img} = req.files;
    const imgName = `avatar${Date.now().toString()}${img.name}`;
    img.mv(path.resolve(__dirname, '..', `uploads/users/${imgName}`), (error) => {
        User.create({
            ...req.body,
            img: `users/${imgName}`

        })
    .then(usr=>{
        console.log("user ajouté !")
        res.json({
            confirmation : "success",
            data:usr
        })
    })
    
    .catch(err=>{
        console.log('user n a pas été ajouté' )
        res.send({
            confirmation:"error",
            data : err.message()
        })
    })
    })
});




router.post('/emp2',(req,res) => {

    console.log(req.body);
    
    Emplois.create(req.body)
    .then(emp => {
        console.log('OK ADD :D ')
        res.json ({
            confirmation : "Success",
            data: emp
        });
    })
    .catch(err => {
        console.log('NOT ADD :( ')
        //console.log(err)
        res.json ({
            confirmation:'FAIL',
            message:err
        });
    })

});


router.post('/emp', (req,res) => {
    Emplois.create(req.body)
    .then(emplois => {
        console.log('ok add')
        res.json({
            confirmation:'success',
            data:emplois
        });
    })
    .catch(err => {
        console.log('oh problem ')
        res.json({
            confirmation:'fail',
            message:err.message()
        });
    });
});




router.get('/getusers', (req,res)=> {
    console.log('getting all users');
    User.find()
    .then(usr=>{
        res.json(usr)
    })
    .catch(err=>{
        res.send(null)
    })
});


router.get('/gett',(req,res) => {
    console.log('getting all timetables');
    Emplois.find({})
    .then(emp => {
        res.json({
            
            data : emp 
        })        
    })
    .catch(err => {
        res.json({
			confirmation:'fail',
			data: err.message()
		})
    })
});



//trouver les postes d'un seul utilisateur
router.get('/getpost/:idusr' , (req,res)=>{
    Pub.find({
        idusr : req.params.idusr 
    })
    .then(pub=>{
        console.log('post de ' + req.params.idusr + ' trouvés') 
        res.json(pub)
    })
    .catch(err=>{
        console.log('error')
        res.send(null)
    })
});



router.get('/getuser/:usrid', (req,res) => {
    User.findOne({
        _id: req.params.usrid
    })
    .then(usr=>{
        o = new Object();

        o._id = usr._id;
        o.nom = usr.nom;
        o.prenom = usr.prenom;
        o.filiere = usr.filiere;
        o.niveau = usr.niveau;
        o.groupe = usr.groupe;
        o.email = usr.email;
        o.img = usr.img;
        res.json(o)
        console.log('success')
    })
    .catch(err=>{
        console.log('erroorr')
        res.send(null)
    })
});

//check login
router.get('/login/:email/:pass', (req,res) => {
    User.findOne({
        email: req.params.email
    })
    .then(usr=>{
        console.log('email trouvé')
        if (req.params.pass == usr.pass)
        {
            console.log('Connexion réussite');
            const o = {
               _id : usr.id,
               email : usr.email,
               nom : usr.nom,
               prenom : usr.prenom,
               filiere : usr.filiere,
               niveau : usr.niveau,
               groupe : usr.groupe,
               img : usr.img};
            res.json(o);
        }
        else {
            console.log('mot de passe incorrecte');
            res.json(null);
        }
    })
    .catch(err=>{
        console.log('erroorr')
        res.send(null)
    })
});



//check login 2
router.post('/login', (req,res) => {
    console.log('chercher connexion')
    User.findOne({
        email: req.body.email
    })
    .then(usr=>{
        console.log('email trouvééé')
        if (req.body.mdp == usr.pass)
        {
            console.log('Connexion réussiteee');
            const o = {
               _id : usr.id,
               email : usr.email,
               nom : usr.nom,
               prenom : usr.prenom,
               filiere : usr.filiere,
               niveau : usr.niveau,
               groupe : usr.groupe,
               img : usr.img};
            res.json(o);
        }
        else {
            console.log('mot de passe incorrecte');
            res.send(null);
        }
    })
    .catch(err=>{
        console.log('erroorr')
        res.send('null')
    })
});




router.get('/getemploi/:filiere/:niveau/:groupe' , (req,res) => {
    console.log('cherche emploi');
    Emplois.findOne({
        filiere : req.params.filiere,
        niveau : req.params.niveau,
        groupe : req.params.groupe
    })
    .then(emp => {
        console.log('emploi trouvé');
        res.json(emp)
    })
    .catch(err => {
        console.log('erreur')
        res.send(null)
    })
});




//try crawler
router.get('/getisgnews', (req,res) => {
    request("http://www.isgs.rnu.tn/", (err,resp,body) => {
    if (err) {
        console.log(err);
        res.send('null')
    }
    else{
        let $ = cheerio.load(body);
       
        const titreEtLien = $('.two_third .clearfix ul li div a').get();
        const img = $('.two_third .clearfix ul li a img').get(0);
        const date = $('.two_third .clearfix ul li div div span strong').get();
        const txt = $('.two_third .clearfix ul li div p').get();
        
        
        let allNews = new Array();
        
            let i=0;
            for (i=0 ; i<titreEtLien.length ; i++ ) {
                let o = new Object();
                o.titre=titreEtLien[i].attribs.title;
                o.lien=titreEtLien[i].attribs.href;
                o.date=date[i].children[0].data;
                o.txt=txt[i].children[0].data;

                allNews.push(o);
            }
            res.json(allNews);
           
        }
                
            
    }
)
})


//vérifier qu'il existe des nouveaux actualités ou non 
router.post('/getisgnews/:title', (req,res) => {
    console.log('chercher des nouvelles notifs');
    console.log(req.body);
    request("http://www.isgs.rnu.tn/", (err,resp,body) => {
    if (err) {
        console.log(err);
        res.send('null')
    }
    else{
        let $ = cheerio.load(body);
       
        const titreEtLien = $('.two_third .clearfix ul li div a').get();
        const img = $('.two_third .clearfix ul li a img').get(0);
        const date = $('.two_third .clearfix ul li div div span strong').get();
        const txt = $('.two_third .clearfix ul li div p').get();
        
        
        let allNews = new Array();
        let newNews = new Array();
          
              
        
            let i=0;
            let position=titreEtLien.length;
            for (i=0 ; i<titreEtLien.length ; i++ ) {
                let o = new Object();
                o.titre=titreEtLien[i].attribs.title;
                o.lien=titreEtLien[i].attribs.href;
                o.date=date[i].children[0].data;
                o.txt=txt[i].children[0].data;

                allNews.push(o);
                if (o.titre == req.body.title) {
                    position = i;
                }
            }

            
            for (i=0 ; i<position ; i++) {
                newNews.push(allNews[i]);
            }
            
            
            res.json(newNews);
          
        }
                
            
    }
)
})




//try to find all user's posts 
router.post('/findpostuser' , (req,res) => {
    console.log('recherche des publications d un utilisateur ');
    let tab = new Array();
    Pub.find({
        idusr : req.body.iduser
    })
    .then(pst=>{
        res.json(pst)
    })
    .catch(err=>{
        res.send('null')
    })
});


router.post('/findpostcomment' , (req,res) => {
    console.log('recherche des nouveaux commentaires ');
    Comm.find({
        idpost : req.body.idpost
    })
    .then(cmnt=>{
        console.log("Taille" + cmnt.length +"   " + req.body.idcomment);
        if (cmnt.length == 0 ) {
            //res.send('null');
            console.log('pas de nouveau commentaires')
            
        }
        else if (cmnt[cmnt.length-1]._id == req.body.idcomment  ) {
            ///res.send('null');
            console.log('pas de nouveau commentaires')
        }
        
        else {
            res.json(cmnt[cmnt.length-1])
            console.log('un nouveau commentaire trouvé')
        }
    })
    .catch(err=>{
        res.send('null')
        console.log('erreur');
    })
});




router.post('/findpostlike' , (req,res) => {
    console.log('recherche des nouveaux commentaires ');
    Comm.find({
        idpost : req.body.idpost
    })
    .then(cmnt=>{
        console.log("Taille" + cmnt.length +"   " + req.body.idcomment);
        if (cmnt.length == 0 ) {
            //res.send('null');
            console.log('pas de nouveau commentaires')
            
        }
        else if (cmnt[cmnt.length-1]._id == req.body.idcomment  ) {
            ///res.send('null');
            console.log('pas de nouveau commentaires')
        }
        
        else {
            res.json(cmnt[cmnt.length-1])
            console.log('un nouveau commentaire trouvé')
        }
    })
    .catch(err=>{
        res.send('null')
        console.log('erreur');
    })
});





router.get('/getpostbyid/:idp' , (req,res) => {

    console.log('find one post');
    Pub.findOne({
        _id: req.params.idp
    })
    .then(pst => {
        res.json(pst)
        console.log(pst);
        console.log('post trouvé')
    })
    .catch(err => {
        res.send('null')
        console.log('post non trouvé')
    })
}); 


router.get('/searchuser/:txt', (req,res) => {
    console.log('search user')
    User.find({
       $or: [ 
           {nom : { '$regex' : req.params.txt, '$options' : 'i' }},
           {prenom : { '$regex' : req.params.txt, '$options' : 'i' }},
           {email : { '$regex' : req.params.txt, '$options' : 'i' }},
           {filiere : { '$regex' : req.params.txt, '$options' : 'i' }}
         ]
       //prenom : new RegExp(req.params.txt, 'i'), 
       //filiere : new RegExp(req.params.txt, 'i')
    })
    .then(u=>{
        let list = new Array();
        let i=0;
        for ( i ; i<u.length; i++) {
            o = new Object()
            o.nom = u[i].nom;
            o.prenom = u[i].prenom;
            o.id = u[i]._id;
            o.img = u[i].img;
            o.email = u[i].email;
            list.push(o);
        }
        res.json(list);
    })
    .catch(err=>{
        res.send(err.message);
    })
})



module.exports = router

