const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Post = new Schema ({

    txtpost:String , 
    idusr:String,
    datepost:String,
    imgpost:[String],
    likes : [String]

});

module.exports = mongoose.model('Post',Post);