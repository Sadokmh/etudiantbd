const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const Comment = new Schema({
    txtcom : String,
    datecom : String,
    idusr : String,
    idpost : String
});

module.exports = mongoose.model('Comment',Comment);