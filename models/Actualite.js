const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Actualite = new Schema({
    titre : String ,
    text : String , 
    date : String , 
    lien : String , 
});

module.exports = mongoose.model('Actualite',Actualite);