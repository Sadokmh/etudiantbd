const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//const seance = mongoose.Model('seance');

const seance=new Schema({
    numseance : Number, 
    mat : String, 
    enseignant : String,
    salle : String , 
    type : String,
    pq : Boolean
});

const jour=new Schema({
    nom : String,
    seances : {
        type : [seance]
    }
    //num:Number
});


const emplois = new Schema ({
    filiere : String,
    niveau : Number,
    groupe: Number,
    jours: {
        type : [jour]
    }

});

module.exports=mongoose.model('emplois',emplois);