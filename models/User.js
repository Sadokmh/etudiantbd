const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const User = new Schema ({

    nom:String , 
    prenom:String,
    email:String,
    pass:String,
    img:String,
    filiere:String,
    groupe:Number,
    niveau:Number

});

module.exports = mongoose.model('User',User);